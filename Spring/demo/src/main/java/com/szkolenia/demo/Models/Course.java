package com.szkolenia.demo.Models;

import com.szkolenia.demo.Domain.Models.CourseType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course {
    private String name;
    private String description;
    private CourseType courseType;

}
