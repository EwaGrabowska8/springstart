package com.szkolenia.demo.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private String name;
    private String surname;
    private String email;
    private LocalDate dateOfBirth;

}
