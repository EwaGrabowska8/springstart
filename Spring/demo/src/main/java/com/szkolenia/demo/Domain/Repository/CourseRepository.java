package com.szkolenia.demo.Domain.Repository;

import com.szkolenia.demo.Domain.Models.CourseEntity;
import com.szkolenia.demo.Models.Course;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CourseRepository extends CrudRepository<CourseEntity,Long> {
    List<CourseEntity> findAllByName(String name);
}
