package com.szkolenia.demo.Domain.Models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@NoArgsConstructor
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "YOUR_ENTITY_SEQ_Student")
    @SequenceGenerator(name = "YOUR_ENTITY_SEQ_Student", sequenceName = "YOUR_ENTITY_SEQ_Student", allocationSize = 1)
    private long id;

    private String name;
    private String surname;
    private String email;
    private LocalDate dateOfBirth;

    @ManyToOne
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private CourseEntity courseEntity;

    public StudentEntity(String name, String surname, String email, LocalDate dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
    }
}
