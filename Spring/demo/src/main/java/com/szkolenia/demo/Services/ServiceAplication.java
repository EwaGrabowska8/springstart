package com.szkolenia.demo.Services;

import com.szkolenia.demo.Domain.Models.CourseEntity;
import com.szkolenia.demo.Domain.Models.StudentEntity;
import com.szkolenia.demo.Domain.Repository.CourseRepository;
import com.szkolenia.demo.Domain.Repository.StudentRepository;
import com.szkolenia.demo.Models.Course;
import com.szkolenia.demo.Models.Student;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@Data
@RequiredArgsConstructor
public class ServiceAplication {
    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;


    public long addStudent(Student student) {
        StudentEntity studentEntity = studentRepository.save(this.map(student));
        return studentEntity.getId();
    }
    //Dodawanie nowej grupy szkoleniowej
    public long addCourse(Course course){
        CourseEntity courseEntity = courseRepository.save(this.map(course));
        return courseEntity.getId();
    }

    public Optional<Student> getStudentById(Long id){
        Optional<StudentEntity> studentEntityOptional = studentRepository.findById(id);
        return studentEntityOptional.map(this::map);
    }

    public Optional<Course> getCourseById(Long id){
        Optional<CourseEntity> coursEntityOptional = courseRepository.findById(id);
        return coursEntityOptional.map(this::map);
    }

    private StudentEntity map(Student student){
        StudentEntity studentEntity = new StudentEntity();
        studentEntity.setName(student.getName());
        studentEntity.setSurname(student.getSurname());
        studentEntity.setDateOfBirth(student.getDateOfBirth());
        studentEntity.setEmail(student.getEmail());
        return studentEntity;
    }

    private Student map(StudentEntity studentEntity){
        Student student = new Student();
        student.setName(studentEntity.getName());
        student.setSurname(studentEntity.getSurname());
        student.setDateOfBirth(studentEntity.getDateOfBirth());
        student.setEmail(studentEntity.getEmail());
        return student;
    }
    private Course map(CourseEntity courseEntity){
        Course course = new Course();
        course.setCourseType(courseEntity.getCourseType());
        course.setDescription(courseEntity.getDescription());
        course.setName(courseEntity.getName());
        return course;
    }
    private CourseEntity map(Course course){
        CourseEntity courseEntity = new CourseEntity();
        courseEntity.setCourseType(course.getCourseType());
        courseEntity.setDescription(course.getDescription());
        courseEntity.setName(course.getName());
        return courseEntity;
    }

    public List<Student> getAllStudents() {
        List<Student> allStudents = StreamSupport.stream(studentRepository.findAll().spliterator(),false)
                .map(this::map)
                .collect(Collectors.toList());
        return allStudents;
    }

    public List<Course> getAllCourses() {
        List<Course> allCourses = StreamSupport.stream(courseRepository.findAll().spliterator(),false)
                .map(this::map)
                .collect(Collectors.toList());
        return allCourses;
    }

    //Usuwanie grupy na podstawie jej nazwy ( bazy danych usunięci zostają również kursanci)
    public List<Course> deleteCourseWithStudents(String name) {
        List<Course> deleteList = new ArrayList<>();
        StreamSupport.stream(courseRepository.findAll().spliterator(),false)
                .filter(courseEntity -> courseEntity.getName().equals(name))
                .forEach(courseEntity ->{
                    deleteList.add(this.map(courseEntity));
                    courseRepository.delete(courseEntity);
                });
        return deleteList;
    }
    //Dodanie nowego kursanta do grupy szkoleniowej
    //Zmianę grupy kursanta
    public boolean setCourse(long studentId, long courseId) {
        Optional<StudentEntity> maybeStudentEntity = studentRepository.findById(studentId);
        Optional<CourseEntity> maybeCourseEntity = courseRepository.findById(courseId);
        if (maybeStudentEntity.isPresent()&&maybeCourseEntity.isPresent()){
            CourseEntity courseEntity = maybeCourseEntity.get();
            StudentEntity studentEntity =maybeStudentEntity.get();
            studentEntity.setCourseEntity(courseEntity);
            studentRepository.save(studentEntity);
            return true;
        }
        return false;
    }
    //Wyświetlenie wszystkich grup wraz z liczbą kursantów w każdej z nich
    public Map<Course, Long> getAllCoursesWithCount() {
        Map<Course,Long> courseMap= StreamSupport.stream(courseRepository.findAll().spliterator(),false)
                .collect(Collectors.toMap(courseEntity -> this.map(courseEntity), courseEntity->Long.valueOf(courseEntity.getStudentSet().size())));

//        Map<Course,Long> courseMap = getAllCourses().stream()
//                .collect(Collectors.toMap(Function.identity(),c->Long.valueOf(0)));
//        getAllStudents().stream()
//                .map(student -> student.getCourse())
//                .filter(Objects::nonNull)
//                .forEach(course -> courseMap.computeIfPresent(course,((k,v)->v+1)));
        return courseMap;
    }
    //Wyświetlenie wszystkich kursantów danej grupy
    public Optional<List<Student>> getStudentListByCourseId(Long id) {
        Optional<CourseEntity> courseById = courseRepository.findById(id);
        if (courseById.isPresent()){
            List<Student> studentList = courseById.get().getStudentSet().stream()
                    .map(this::map)
                    .collect(Collectors.toList());
            return Optional.of(studentList);
        }
        return Optional.empty();
    }
    //Usunięcie kursanta z bazy
    public Optional<Student> deleteStudent(Long id) {
        Optional<StudentEntity> studentById = studentRepository.findById(id);
        studentById.ifPresent(studentEntity -> studentRepository.delete(studentEntity));
        return studentById.map(this::map);
    }
    //Wyświetlenie wszystkich kursantów mających dzisiaj urodziny (wraz z informacjami o ich grupie szkoleniowej)
    public Map<Student, Course> getStudentByDate(LocalDate date) {
        String monthDay = date.format(DateTimeFormatter.ofPattern("MM-dd"));
        List<StudentEntity> byDateOfBirth = studentRepository.findByDateOfBirth(monthDay);
        Map<Student, Course> studentCourseMap = new HashMap<>();
        byDateOfBirth.forEach(studentEntity -> {
            if (studentEntity.getCourseEntity()!=null){
                studentCourseMap.put(this.map(studentEntity),this.map(studentEntity.getCourseEntity()));
            }else {
                studentCourseMap.put(this.map(studentEntity),null);
            }
        });
        return studentCourseMap;
    }

}
