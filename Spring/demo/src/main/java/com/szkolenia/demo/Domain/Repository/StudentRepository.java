package com.szkolenia.demo.Domain.Repository;

import com.szkolenia.demo.Domain.Models.StudentEntity;
import com.szkolenia.demo.Models.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface StudentRepository extends CrudRepository<StudentEntity, Long> {
    List<Student> findAllByName (String name);

    @Query(value = "select * from Student_Entity where date_of_Birth like %?1",
    nativeQuery = true)
    List<StudentEntity> findByDateOfBirth(String date);
}
