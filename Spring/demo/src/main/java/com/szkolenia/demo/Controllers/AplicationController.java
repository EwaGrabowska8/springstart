package com.szkolenia.demo.Controllers;

import com.szkolenia.demo.Domain.Models.CourseType;
import com.szkolenia.demo.Models.Course;
import com.szkolenia.demo.Models.Student;
import com.szkolenia.demo.Services.ServiceAplication;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class AplicationController {
    private final ServiceAplication serviceAplication;

    @PostMapping(path = "/addStudent")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Long addStudent(@RequestBody Student student) {
        long id = serviceAplication.addStudent(student);
        return id;
    }

    @GetMapping("/getStudent")
    public ResponseEntity<Student> getStident(@RequestParam ("id") Long id){
        Optional<Student> studentById = serviceAplication.getStudentById(id);
        return studentById.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
    //Wyświetlenie wszystkich kursantów danej grupy
    @GetMapping("/getStudentOfCourse")
    public ResponseEntity<List<Student>> getStidentOfCourse(@RequestParam ("id") Long id){
        Optional<List<Student>> studentlistByCourseId = serviceAplication.getStudentListByCourseId(id);
        return studentlistByCourseId.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
    @GetMapping("/getStudent/getAll")
    public ResponseEntity<List<Student>> getAllStident(){
        List<Student> studentsList = serviceAplication.getAllStudents();
        return ResponseEntity.ok(studentsList);
    }

    //Dodawanie nowej grupy szkoleniowej
    @PostMapping("/addCourse")
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<Long> addCourse(@RequestBody Course course) {
        long id = serviceAplication.addCourse(course);
        return ResponseEntity.ok(id);
    }
    @GetMapping("/getCourse")
    public ResponseEntity<Course> getCourse(@RequestParam ("id") Long id){
        Optional<Course> courseById = serviceAplication.getCourseById(id);
        return courseById.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
    @GetMapping("/getCourse/getAll")
    public ResponseEntity<List<Course>> getAllCourse(){
        List<Course> coursesList = serviceAplication.getAllCourses();
        return ResponseEntity.ok(coursesList);
    }
    //Wyświetlenie wszystkich grup wraz z liczbą kursantów w każdej z nich
    @GetMapping("/getCourse/getAllWithStudentsCount")
    public ResponseEntity<Map<Course,Long>> getAllCourseWithStudentsCount(){
        Map<Course, Long> coursesListWithCount = serviceAplication.getAllCoursesWithCount();
        return ResponseEntity.ok(coursesListWithCount);
    }
    //Usuwanie grupy na podstawie jej nazwy ( bazy danych usunięci zostają również kursanci)
    @DeleteMapping("/deleteCourse")
    public ResponseEntity<List<Course>> deleteCourseWittStudents(@RequestParam ("name") String name){
        List<Course> deleteCourseWithStudents = serviceAplication.deleteCourseWithStudents(name);
        if (deleteCourseWithStudents.size()>0){
            return ResponseEntity.ok(deleteCourseWithStudents);
        }
        return ResponseEntity.notFound().build();
    }
    //Dodanie nowego kursanta do grupy szkoleniowej
    //Zmianę grupy kursanta
    @PutMapping("/setCourse")
    public ResponseEntity setCourse(@RequestParam ("studentId") long studentId, @RequestParam("courseId") long courseId){
        boolean saveSucces = serviceAplication.setCourse(studentId, courseId);
        if (saveSucces) return ResponseEntity.ok().build();
        else return ResponseEntity.notFound().build();
    }
    //Usunięcie kursanta z bazy
    @DeleteMapping("/deleteStudent")
    public ResponseEntity<Student> deleteStudent(@RequestParam ("id") Long id){
        Optional<Student> deleteStudent = serviceAplication.deleteStudent(id);
        ResponseEntity<Student> studentResponseEntity = deleteStudent.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
        return studentResponseEntity;
    }
    //Wyświetlenie wszystkich kursantów mających dzisiaj urodziny (wraz z informacjami o ich grupie szkoleniowej)
    @GetMapping("/getStudentsByDate")
    public ResponseEntity<Map<Student,Course>> getStudentByDate(){
        Map<Student,Course> studentCourseMap = serviceAplication.getStudentByDate(LocalDate.now());
        return ResponseEntity.ok(studentCourseMap);
    }

    @GetMapping("/init")
    public void initDataBase(){
        Course course = new Course("java","bla bla bla", CourseType.WEEKENDOWY);
        Course course2 = new Course("testy","bla bla bla", CourseType.DZIENNY);
        Course course3 = new Course("frontEnd","bla bla bla", CourseType.WIECZOROWY);

        serviceAplication.addCourse(course);
        serviceAplication.addCourse(course2);
        serviceAplication.addCourse(course3);

        Student student = new Student("Ewa","Grabowska","email@email", LocalDate.of(1984,1,14));
        Student student2 = new Student("Justyna","Dziuba","justyna@email", LocalDate.now());
        Student student3 = new Student("Ewa","Grabowska","emilka@email", LocalDate.of(1984,11,2));

        serviceAplication.addStudent(student);
        serviceAplication.addStudent(student2);
        serviceAplication.addStudent(student3);

    }


}
