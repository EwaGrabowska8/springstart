package com.szkolenia.demo.Domain.Models;

import lombok.*;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class CourseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "YOUR_ENTITY_SEQ_Course")
    @SequenceGenerator(name = "YOUR_ENTITY_SEQ_Course", sequenceName = "YOUR_ENTITY_SEQ_Course", allocationSize = 1)
    private long id;

    private String name;
    private String description;
    private CourseType courseType;

    @OneToMany(mappedBy = "courseEntity", fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Set<StudentEntity > studentSet = new HashSet<>();

}
