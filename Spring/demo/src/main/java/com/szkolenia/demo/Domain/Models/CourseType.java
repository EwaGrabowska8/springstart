package com.szkolenia.demo.Domain.Models;

public enum CourseType {
    DZIENNY, WEEKENDOWY, WIECZOROWY
}
